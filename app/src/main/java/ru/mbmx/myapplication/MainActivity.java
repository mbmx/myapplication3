package ru.mbmx.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ClassCounter CCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(getApplicationContext(), "Counter = " + String.valueOf(CCounter.getCounter()), Toast.LENGTH_SHORT).show();

        CCounter = new ClassCounter();
    }

    public void ClickBt1(View v) {
        //Toast.makeText(getApplicationContext(), "Btn1", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);
    }

    public void ClickBt2(View v) {
        //Toast.makeText(getApplicationContext(), "Btn2", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, Main3Activity.class);
        startActivity(intent);
    }

    public void ClickBt3(View v) {
        Toast.makeText(getApplicationContext(), "Counter = " + String.valueOf(CCounter.getCounter()), Toast.LENGTH_SHORT).show();
    }
}
