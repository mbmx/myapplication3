package ru.mbmx.myapplication;

public final class ClassCounter {

    private static int counter;

    public static void onCreate() {
        counter = 0;
    }

    public static void inc() {
        counter = counter + 1;
    }

    public static int getCounter() {
        return counter;
    }
}
